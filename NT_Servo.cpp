/*
 * @file NT_Motor.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Motor function definitions.
 */

#include "Arduino.h"
#include "NT_Servo.h"

Servo_t::Servo_t()
{
}

void Servo_t::Configure(int Startup_Sp, int Deadband_Sp, int Minimum_Sp, int Maximum_Sp, int Minimum_Op, int Maximum_Op)
{
	Startup_Sp = Startup_Sp;
	Deadband_Sp = Deadband_Sp;
	Minimum_Sp = Minimum_Sp;
	Maximum_Sp = Maximum_Sp;
	Minimum_Op = Minimum_Op;
	Maximum_Op = Maximum_Op;
}

void Servo_t::Init()
{
	Enable = TRUE;
}

void Servo_t::Deinit()
{
	Enable = FALSE;
}

void Servo_t::Set(int Setpoint)
{
	if (Enable == TRUE)
	{
		float Difference;

		// Check if difference between new and old value is more than the defined deadband
		Difference = abs(Value_Sp - Setpoint);

		// Set new pwm value if difference is more than defined deadband
		if (Difference >= Deadband_Sp)
		{
			// Roof data if below or over limits
			if (Setpoint > Maximum_Sp)
			{
				Value_Sp = Maximum_Sp;
			}
			else if (Setpoint < Minimum_Sp)
			{
				Value_Sp = Minimum_Sp;
			}

			// Map value of setpoint to correct duty cycle values
			Value_Op = map(Value_Sp, 0, Maximum_Sp, Minimum_Op, Maximum_Op);
		}
	}
	else
	{
		Value_Sp = Startup_Sp;
		Value_Op = map(Value_Sp, 0, Maximum_Sp, Minimum_Op, Maximum_Op);
	}
}