/*
 * @file NT_Servo_Motor.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Motor function declarations.
 */

#ifndef NT_Servo_Motor_h

#include "Arduino.h"
#include "NT_Pwm.h"

#define TRUE 1
#define FALSE 0

#define NT_Servo_Motor_h

class Servo_t
{
public:
	Servo_t();
	bool Enable = FALSE;
	int Value_Sp = 0;
	int Startup_Sp = 0;
	int Deadband_Sp = 0;
	int Minimum_Sp = 0;
	int Maximum_Sp = 0;
	int Value_Op = 0;
	int Minimum_Op = 0;
	int Maximum_Op = 0;
	void Configure(int Startup_Sp, int Deadband_Sp, int Minimum_Sp, int Maximum_Sp, int Minimum_Op, int Maximum_Op);
	void Init();
	void Deinit();
	void Set(int Setpoint);
};

#endif