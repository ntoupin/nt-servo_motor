/*
   @file Servo.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Simple example of servo motor utilisation.
*/

#include <NT_Pwm.h>
#include <NT_Servo.h>

Pwm_t Servo1_Pwm1;
Servo_t Servo1;

void setup()
{
  Servo1.Configure(0, 0, 0, 1000, 0, 256);
  Servo1.Init();

  Servo1_Pwm1.Configure(0, 0, Servo1.Minimum_Op, Servo1.Maximum_Op, 0, 255);
  Servo1_Pwm1.Attach(9); // Set pin of Pwm
  Servo1_Pwm1.Init();
}

void loop()
{
  for (int i = Servo1.Minimum_Sp; i < Servo1.Maximum_Sp; i += 5)
  {
    Servo1.Set(i);
    Servo1_Pwm1.Set(Servo1.Value_Op);

    delay(10);
  }
}
